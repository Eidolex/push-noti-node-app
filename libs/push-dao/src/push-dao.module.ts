import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AndroidConfigService, AndroidConfigScheme } from './android-config';
import {
  AndroidScheduleService,
  AndroidScheduleScheme,
} from './android-schedule';
import { AndroidTokenService, AndroidTokenScheme } from './android-token';
import { UserScheme, UserService } from './user';
import { DBService } from './db';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://mongo:27017/nest', {
      user: 'root',
      pass: 'password',
    }),
    MongooseModule.forFeature([
      { name: 'AndroidConfig', schema: AndroidConfigScheme },
      { name: 'AndroidSchedule', schema: AndroidScheduleScheme },
      { name: 'AndroidToken', schema: AndroidTokenScheme },
      { name: 'User', schema: UserScheme },
    ]),
  ],
  providers: [
    AndroidConfigService,
    AndroidScheduleService,
    AndroidTokenService,
    UserService,
    DBService,
  ],
  exports: [
    AndroidConfigService,
    AndroidScheduleService,
    AndroidTokenService,
    UserService,
    DBService,
  ],
})
export class PushDaoModule {}
