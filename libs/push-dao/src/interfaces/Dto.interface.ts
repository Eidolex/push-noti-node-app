import { Document } from 'mongoose';

export type Dto<T> = Omit<T, keyof Document>;

export type DtoOmit<T, K extends keyof Dto<T>> = Omit<Dto<T>, K>;

export type DtoWith<T, K extends keyof Document> = Omit<
  T,
  Exclude<keyof Document, K>
>;
