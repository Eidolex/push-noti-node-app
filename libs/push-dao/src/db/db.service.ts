import { Injectable } from '@nestjs/common';
import { startSession } from 'mongoose';
import { Transaction } from './interfaces/common.interface';

@Injectable()
export class DBService {
  async startTransaction(): Promise<Transaction> {
    return startSession();
  }

  async commit(transaction: Transaction): Promise<void> {
    await transaction.commitTransaction();
    await transaction.endSession();
  }

  async rollback(transaction: Transaction): Promise<void> {
    await transaction.abortTransaction();
    await transaction.endSession();
  }
}
