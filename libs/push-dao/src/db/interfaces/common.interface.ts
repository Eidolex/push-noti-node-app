import { ClientSession, Schema } from 'mongoose';

export type Transaction = ClientSession;
export type Id = Schema.Types.ObjectId | string;
