import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
  _id: false,
})
class AndroidScheduleData {
  @Prop({
    type: String,
    required: true,
  })
  title: string;

  @Prop({
    type: String,
    required: true,
  })
  message: string;

  @Prop({
    type: Object,
    validate: {
      validator: (val?: { [key: string]: any }) => {
        if (val && Object.keys(val).length > 15) {
          let result = false;
          for (const k in val) {
            const v = val[k];
            // check nested object
            if (typeof v == 'object') {
              result = false;
              break;
            }
            // check is array
            // check array length is less then 10
            // check array contain nested array or object
            if (
              Array.isArray(v) &&
              (v.length > 10 ||
                v.filter(
                  l =>
                    typeof l === 'object' ||
                    typeof Array.isArray(l) ||
                    (typeof l === 'string' && l.length > 50),
                ).length > 0)
            ) {
              result = false;
              break;
            }
          }
          return result;
        }
        return true;
      },
      message: props =>
        `${props.path} can't had more then 15 key, doesn't support nested object and if it's array can only contain more then 10 item with maxlength 50`,
    },
  })
  customKeys?: { [key: string]: any };
}

const AndroidScheduleDataScheme = SchemaFactory.createForClass(
  AndroidScheduleData,
);

export { AndroidScheduleData, AndroidScheduleDataScheme };
