import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongoSchema } from 'mongoose';
import {
  AndroidScheduleData,
  AndroidScheduleDataScheme,
} from './android-schedule-data.schema';
import { Id } from '@app/push-dao/db';

@Schema()
export class AndroidSchedule extends Document {
  // timestamp the job will start
  @Prop({
    type: Date,
    required: true,
  })
  doScheduleAt: Date;

  // timestamp the job completed
  @Prop({
    type: Date,
  })
  doneScheduleAt?: Date;

  @Prop({
    type: Date,
  })
  cancelScheduleAt?: Date;

  @Prop({
    type: AndroidScheduleDataScheme,
    required: true,
  })
  data: AndroidScheduleData;

  @Prop({
    type: MongoSchema.Types.ObjectId,
    required: true,
    index: true,
  })
  androidConfigId: Id;
}

// export type AndroidConfigDto = Omit<AndroidConfig, keyof Document>

export const AndroidScheduleScheme = SchemaFactory.createForClass(
  AndroidSchedule,
);
