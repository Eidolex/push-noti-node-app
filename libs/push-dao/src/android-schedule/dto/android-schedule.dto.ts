import { Id } from '@app/push-dao';

export type CreateAndroidScheduleDto = {
  doScheduleAt: Date;
  data: {
    title: string;
    message: string;
    customKeys: { [key: string]: any };
  };
  androidConfigId: Id;
};

export type AndroidScheduleDto = CreateAndroidScheduleDto & {
  _id: Id;
  doneScheduleAt: Date;
  cancelScheduleAt: Date;
};

export type FindAndroidScheduleDto = Partial<AndroidScheduleDto>;

export type UpdateAndroidScheduleDto = Omit<FindAndroidScheduleDto, '_id'>;
