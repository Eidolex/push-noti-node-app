import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AndroidSchedule } from './schemas/android-schedule.schema';
import { Model } from 'mongoose';
import { Transaction, Id } from '../db';
import {
  CreateAndroidScheduleDto,
  FindAndroidScheduleDto,
  UpdateAndroidScheduleDto,
} from './dto/android-schedule.dto';

@Injectable()
export class AndroidScheduleService {
  constructor(
    @InjectModel('AndroidSchedule')
    private readonly androidScheduleModel: Model<AndroidSchedule>,
  ) {}

  public async createAndroidSechedule(
    androidSchedule: CreateAndroidScheduleDto,
    session?: Transaction,
  ): Promise<AndroidSchedule> {
    const androidScheduleModel = new this.androidScheduleModel(androidSchedule);
    return androidScheduleModel.save({
      session,
      validateBeforeSave: true,
    });
  }

  public async findAndroidSchedules(
    filter: FindAndroidScheduleDto,
  ): Promise<AndroidSchedule[]> {
    return this.androidScheduleModel.find(filter).exec();
  }

  public async updateAndroidScheduleById(
    _id: Id,
    data: UpdateAndroidScheduleDto,
    session?: Transaction,
  ): Promise<AndroidSchedule> {
    return this.androidScheduleModel.updateOne({ _id }, data, {
      session,
    });
  }
}
