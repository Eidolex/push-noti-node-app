import { Test, TestingModule } from '@nestjs/testing';
import { AndroidScheduleService } from './android-schedule.service';

describe('AndroidScheduleService', () => {
  let service: AndroidScheduleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AndroidScheduleService],
    }).compile();

    service = module.get<AndroidScheduleService>(AndroidScheduleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
