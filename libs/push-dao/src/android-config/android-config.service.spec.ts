import { Test, TestingModule } from '@nestjs/testing';
import { AndroidConfigService } from './android-config.service';

describe('AndroidConfigService', () => {
  let service: AndroidConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AndroidConfigService],
    }).compile();

    service = module.get<AndroidConfigService>(AndroidConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
