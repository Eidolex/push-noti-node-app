import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongoSchema } from 'mongoose';
import { Id } from '@app/push-dao/db';

@Schema({
  timestamps: true,
})
export class AndroidConfig extends Document {
  @Prop({
    type: String,
    required: true,
    unique: true,
  })
  name: string;

  /**
   * the firebase server key
   * use for sending push with legeacy api
   */
  @Prop({
    type: String,
    required: true,
  })
  authKey: string;

  /**
   * the firebase project id
   * use for sending bulk message with new api
   */
  @Prop({
    type: String,
    required: true,
  })
  projectId: string;

  /**
   *  the firebase service private key
   *  use for generating auth bearer key
   */
  @Prop({
    type: String,
    required: true,
  })
  privateKey: string;

  /**
   *  the firebase service private key
   *  use for generating auth bearer key
   */
  @Prop({
    type: String,
    required: true,
  })
  clientEmail: string;

  /**
   *
   */
  @Prop({
    type: MongoSchema.Types.ObjectId,
    required: true,
  })
  userId: Id;
}

// export type AndroidConfigDto = Omit<AndroidConfig, keyof Document>

export const AndroidConfigScheme = SchemaFactory.createForClass(AndroidConfig);
