import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AndroidConfig } from './schemas/android-config.schema';
import { Model } from 'mongoose';
import { Dto } from '../interfaces/Dto.interface';
import { Transaction, Id } from '../db';

@Injectable()
export class AndroidConfigService {
  constructor(
    @InjectModel('AndroidConfig')
    private readonly androidConfigModel: Model<AndroidConfig>,
  ) {}

  public async createAndroidConfig(
    androidConfig: Dto<AndroidConfig>,
    session?: Transaction,
  ): Promise<AndroidConfig> {
    const androidModel = new this.androidConfigModel(androidConfig);
    return androidModel.save({
      validateBeforeSave: true,
      session,
    });
  }

  public async updateAndroidConfigById(
    _id: Id,
    androidConfig: Dto<AndroidConfig>,
    session?: Transaction,
  ): Promise<AndroidConfig> {
    return this.androidConfigModel.updateOne({ _id }, androidConfig, {
      runValidators: true,
      session,
    });
  }

  public async findAndroidConfigByUserId(userId: Id): Promise<AndroidConfig[]> {
    return this.androidConfigModel
      .find({
        userId,
      })
      .exec();
  }

  public async findAndroidConfigByName(name: string): Promise<AndroidConfig> {
    return await this.androidConfigModel.findOne({ name }).exec();
  }

  public decodeConfigKey(configKey: string): string {
    return configKey;
  }

  public encodeConfigKey(configName: string): string {
    return configName;
  }
}
