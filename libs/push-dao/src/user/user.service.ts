import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { User } from './schemas/user.schema';
import { Dto } from '../interfaces/Dto.interface';
import { Transaction, Id } from '../db';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User')
    private readonly userModel: Model<User>,
  ) {}

  public async createUser(
    user: Dto<User>,
    session?: Transaction,
  ): Promise<User> {
    const userModel = new this.userModel(user);
    return userModel.save({
      validateBeforeSave: true,
      session,
    });
  }

  public async updatUserById(
    _id: Id,
    user: Dto<User>,
    session?: Transaction,
  ): Promise<User> {
    return this.userModel.updateOne({ _id }, user, {
      runValidators: true,
      session,
    });
  }

  public async findUserByEmail(email: string): Promise<User> {
    return this.userModel
      .findOne({
        email,
      })
      .exec();
  }

  public async findUserById(_id: Id): Promise<User> {
    return this.userModel.findById(_id).exec();
  }
}
