import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class User extends Document {
  /**
   *  the firebase service private key
   *  use for generating auth bearer key
   */
  @Prop({
    type: String,
    required: true,
    unique: true,
  })
  email: string;

  /**
   *  the firebase service private key
   *  use for generating auth bearer key
   */
  @Prop({
    type: String,
    required: true,
  })
  password: string;

  /**
   *
   */
  // @Prop({
  //   type: MongoSchema.Types.ObjectId,
  //   required: true,
  // })
  // userId: MongoSchema.Types.ObjectId;
}

// export type AndroidConfigDto = Omit<AndroidConfig, keyof Document>

export const UserScheme = SchemaFactory.createForClass(User);
