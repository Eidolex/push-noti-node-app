import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongoSchema } from 'mongoose';
import { Id } from '@app/push-dao/db';

@Schema()
export class AndroidToken extends Document {
  // timestamp the job will start

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  appUserId: string;

  @Prop({
    type: String,
    required: true,
  })
  deviceToken: string;

  @Prop({
    type: [String],
  })
  categoryIds?: string[];

  @Prop({
    type: MongoSchema.Types.ObjectId,
    required: true,
    index: true,
  })
  androidConfigId: Id;
}

// export type AndroidConfigDto = Omit<AndroidConfig, keyof Document>

export const AndroidTokenScheme = SchemaFactory.createForClass(AndroidToken);
