import { Injectable } from '@nestjs/common';
import {
  CreateAndroidTokenDto,
  DeleteAndroidTokenDto,
} from './dto/android-token.dto';
import { AndroidToken } from './schemas/android-token.schema';
import { Model } from 'mongoose';
import { Transaction, Id } from '../db';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class AndroidTokenService {
  constructor(
    @InjectModel('AndroidToken')
    private readonly androidTokenModel: Model<AndroidToken>,
  ) {}

  public async createOrUpdateAndroidToken(
    data: CreateAndroidTokenDto,
    session?: Transaction,
  ): Promise<AndroidToken> {
    return this.androidTokenModel.findOneAndUpdate(
      {
        appUserId: data.appUserId,
        androidConfigId: data.androidConfigId,
      },
      data,
      {
        upsert: true,
        session,
      },
    );
  }

  public async deleteAndroidToken(
    filter: DeleteAndroidTokenDto,
    session?: Transaction,
  ) {
    return this.androidTokenModel.findOneAndDelete(filter, {
      session,
    });
  }

  public async findAndroidToken(
    androidConfigId: Id,
    skip?: number,
    limit?: number,
  ): Promise<AndroidToken[]> {
    let query = this.androidTokenModel.find({
      androidConfigId,
    });

    if (skip) {
      query = query.skip(skip);
    }

    if (limit) {
      query = query.limit(limit);
    }

    return query.exec();
  }

  public async findAndroidTokenByAppUserId(
    androidConfigId: Id,
    appUserId: string,
  ): Promise<AndroidToken> {
    return this.androidTokenModel
      .findOne({
        androidConfigId,
        appUserId,
      })
      .exec();
  }

  public async findAndroidTokenByAppUserIds(
    androidConfigId: Id,
    appUserId: string[],
  ): Promise<AndroidToken[]> {
    return this.androidTokenModel
      .find({ androidConfigId })
      .in('appUserId', appUserId)
      .exec();
  }

  public async findAndroidTokenByCategoryIds(
    androidConfigId: Id,
    categoryIds: string[],
    skip?: number,
    limit?: number,
  ): Promise<AndroidToken[]> {
    let query = this.androidTokenModel
      .find({
        androidConfigId,
      })
      .in('categoryIds', categoryIds);

    if (skip) {
      query = query.skip(skip);
    }

    if (limit) {
      query = query.limit(limit);
    }

    return query.exec();
  }
}
