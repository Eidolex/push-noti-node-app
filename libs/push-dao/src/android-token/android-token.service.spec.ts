import { Test, TestingModule } from '@nestjs/testing';
import { AndroidTokenService } from './android-token.service';

describe('AndroidTokenService', () => {
  let service: AndroidTokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AndroidTokenService],
    }).compile();

    service = module.get<AndroidTokenService>(AndroidTokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
