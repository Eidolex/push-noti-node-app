import { Dto } from '@app/push-dao/interfaces/Dto.interface';
import { AndroidToken } from '../schemas/android-token.schema';
import { Id } from '@app/push-dao/db';

export type CreateAndroidTokenDto = Dto<AndroidToken>;

export type DeleteAndroidTokenDto = {
  appUserId: string;
  androidConfigId: Id;
};
