import { Module, HttpModule } from '@nestjs/common';
import { IphonePushService } from './iphone-push/iphone-push.service';
import { FcmHttp2Service } from './android-push/fcm-http2.service';
import { FcmLegacyService } from './android-push/fcm-legacy.service';
import { FirebaseAuthService } from './android-push/firebase-auth.service';

@Module({
  imports: [HttpModule],
  providers: [
    IphonePushService,
    FcmHttp2Service,
    FcmLegacyService,
    FirebaseAuthService,
  ],
  exports: [
    IphonePushService,
    FcmHttp2Service,
    FcmLegacyService,
    FirebaseAuthService,
  ],
})
export class PushNotificationModule {}
