export type BasePushPayload = {
  title: string;
  message: string;
  customKey?: {
    [key: string]: number | string;
  };
};
