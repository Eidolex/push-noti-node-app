import axios, { AxiosResponse } from 'axios';
import { Injectable } from '@nestjs/common';
import { Agent } from 'https';
import {
  ApnsPayload,
  ApnsRequestHeader,
  ApnsRequestBody,
  ApnsResponse,
} from './interfaces/iphone-push.interface';

@Injectable()
export class IphonePushService {
  private readonly sandBoxEndPoint;
  private readonly productionEndPoint;

  constructor() {
    this.sandBoxEndPoint = '';
    this.productionEndPoint = '';
  }

  public push(
    agent: string | Agent | Buffer,
    payload: ApnsPayload,
    sandbox = false,
  ) {
    if (typeof agent == 'string' || agent instanceof Buffer) {
      agent = IphonePushService.constructAgent(agent);
    }
    const data = IphonePushService.constructPayload(payload);
    this.send(agent, payload.deviceToken, data, sandbox);
  }

  public static constructAgent(pemStr: string | Buffer): Agent {
    return new Agent({
      ca: pemStr,
    });
  }

  private static constructPayload(payload: ApnsPayload): ApnsRequestBody {
    const body: ApnsRequestBody = {
      aps: {
        alert: {
          title: payload.title,
          body: payload.message,
        },
      },
    };

    if (payload.badge) {
      body.aps.badge = payload.badge;
    }

    if (payload.sound) {
      body.aps.sound = payload.sound;
    }

    return body;
  }

  private async send(
    agent: Agent,
    token: string,
    payload: ApnsRequestBody,
    sandbox = false,
    headers?: ApnsRequestHeader,
  ): Promise<AxiosResponse<ApnsResponse>> {
    const endPoint = sandbox ? this.sandBoxEndPoint : this.productionEndPoint;

    return axios.post<ApnsResponse>(`${endPoint}/${token}`, payload, {
      httpsAgent: agent,
      headers,
    });
  }
}
