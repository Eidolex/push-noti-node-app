export type ApnsPayload = {
  title: string;
  message: string;
  deviceToken: string;
  badge?: number;
  sound?: string;
};

export type ApnsRequestHeader = {
  authorization?: string;
  // use to identify notification when omit apns will create one
  // eg: 123e4567-e89b-12d3-a456-42665544000
  'apns-id'?: string;

  //A UNIX epoch date expressed in seconds (UTC)
  // if 0 doesn't store and retry if fail
  'apns-expiration'?: number;

  // 10–Send the push message immediately
  // 5—Send the push message at a time that takes into account power considerations for the device
  // default is 10
  'apns-priority'?: 5 | 10;

  'apns-collapse-id': string;

  // require when using token base auth
  'apns-topic'?: string;
};

// body maxsize is 4KB
export type ApnsRequestBody = {
  aps: {
    category?: string; // use for custom action
    alert: ApnsAlert | ApnsAlertWithLocalString | string;
    badge?: number;
    sound?: string; // the sound file name
  };
};

type ApnsCustomField = { [key: string]: string | number | (string | number)[] };

type ApnsAletButton = {
  'action-loc-key'?: string; // display alert with close button with custom text from app
};

type ApnsAlert = {
  title: string;
  body: string;
} & ApnsAletButton;

type ApnsAlertWithLocalString = {
  'loc-key': string; // Localizable.strings file key eg: GAME_PLAY_REQUEST_FORMAT
  'loc-args': string[]; // args for Localizable.strings
} & ApnsAletButton;

export type ApnsResponse = {};
