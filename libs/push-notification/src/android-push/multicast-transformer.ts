import { AxiosRequestConfig, AxiosResponse } from 'axios';

const PART_BOUNDARY = '__END_OF_PART__';

function createPart(
  serializedRequest: string,
  boundary: string,
  idx: number,
): string {
  let part = `--${boundary}\r\n`;
  part += `Content-Length: ${serializedRequest.length}\r\n`;
  part += 'Content-Type: application/http\r\n';
  part += `content-id: ${idx + 1}\r\n`;
  part += 'content-transfer-encoding: binary\r\n';
  part += '\r\n';
  part += `${serializedRequest}\r\n`;
  return part;
}

/**
 * @param request {SubRequest} The sub request to be serialized.
 * @return {string} String representation of the SubRequest.
 */
function serializeRequest(data: any, url: string): string {
  const requestBody: string = JSON.stringify(data);
  let messagePayload = `POST ${url} HTTP/1.1\r\n`;
  messagePayload += `Content-Length: ${requestBody.length}\r\n`;
  messagePayload += 'Content-Type: application/json; charset=UTF-8\r\n';
  messagePayload += '\r\n';
  messagePayload += requestBody;
  return messagePayload;
}

export const multicastRequestInterceptor = (config: AxiosRequestConfig) => {
  const {
    data: { data, url },
  } = config;
  let { headers } = config;

  headers = {
    ...headers,
    post: { 'Content-Type': `multipart/mixed; boundary=${PART_BOUNDARY}` },
  };

  let buffer = '';
  if (Array.isArray(data)) {
    data.forEach((request, idx: number) => {
      buffer += createPart(serializeRequest(request, url), PART_BOUNDARY, idx);
    });
    buffer += `--${PART_BOUNDARY}--\r\n`;
    // data = data.reduce((d, p, idx) => {
    //   const str = serializeRequest(p, url);
    //   return d + createPart(str, PART_BOUNDARY, idx);
    // }, '');
  }
  return {
    ...config,
    headers,
    data: Buffer.from(buffer, 'utf-8'),
  };
};

export const multicastResponseInterceptor = (
  response: AxiosResponse<string>,
) => {
  // TODO: implement muticast response transformer
  return response;
};
