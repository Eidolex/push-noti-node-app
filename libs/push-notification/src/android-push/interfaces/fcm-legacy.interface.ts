import { BasePushPayload } from '../../interfaces/base-push.interface';

export interface FcmLegacyApiHeader {
  'Content-Type': 'application/json';
  Authorization: string;
}

export type FcmLegacyPushPayload = {
  deviceTokens: string[];
} & BasePushPayload;

export interface FcmLegacyApiRequestBody {
  condition?: string;
  data?: any;
  registration_ids: string[] | string;
  collapse_key?: string;
  priority?: 'normal' | 'high'; // default if notification high else data normal
  content_available?: boolean;
  time_to_live?: number; // default 4 week maximun 4 week
  dry_run?: boolean; // default fals
}

export type FcmLegacyApiResponse = {
  multicast_id: string;
  success: number;
  failure: number;
  results: FcmResult[];
};

export type FcmResult = {
  message_id: string;
  error: FcmResultError[];
};

export type FcmResultError = {};
