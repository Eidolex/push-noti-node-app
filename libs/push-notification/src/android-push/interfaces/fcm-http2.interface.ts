import { BasePushPayload } from '../../interfaces/base-push.interface';

export type FcmHttp2PushPayload = {
  deviceToken: string;
} & BasePushPayload;

export type FcmHttp2ApiHeader = {
  Authorization: string;
};

export type FcmHttp2ApiRequestData = {
  message: {
    token: string;
    data?: any;
  };
};

export type FcmHttp2ApiRequestBody = {
  data: FcmHttp2ApiRequestData[];
  url: string;
};

export type FcmHttp2ApiResponse = {};
