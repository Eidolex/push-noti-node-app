import { Injectable, HttpService } from '@nestjs/common';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import {
  FcmLegacyApiHeader,
  FcmLegacyPushPayload,
  FcmLegacyApiRequestBody,
  FcmLegacyApiResponse,
} from './interfaces/fcm-legacy.interface';

const PUSH_END_POINT = 'https://fcm.googleapis.com';

@Injectable()
export class FcmLegacyService {
  constructor(private readonly httpService: HttpService) {}

  public async push(
    authKey: string,
    payload: FcmLegacyPushPayload,
    options?: AxiosRequestConfig,
  ) {
    const headers = FcmLegacyService.constructHeaders(authKey);
    const data = FcmLegacyService.constructPayload(payload);

    options = FcmLegacyService.mergeOptions(headers, options);

    await this.sendToApi(data, options);
  }

  private static constructHeaders(authKey: string): FcmLegacyApiHeader {
    return {
      'Content-Type': 'application/json',
      Authorization: `key=${authKey}`,
    };
  }

  private static constructPayload(
    payload: FcmLegacyPushPayload,
  ): FcmLegacyApiRequestBody {
    const { customKey, deviceTokens, ...rest } = payload;
    return {
      data: {
        ...rest,
        ...customKey,
      },
      // eslint-disable-next-line @typescript-eslint/camelcase
      registration_ids: deviceTokens,
    };
  }

  private static mergeOptions(headers: any, options?: AxiosRequestConfig) {
    options = options || {};
    return {
      ...options,
      headers: {
        ...options.headers,
        ...headers,
      },
    };
  }

  private async sendToApi(
    payload: FcmLegacyApiRequestBody,
    options: any,
  ): Promise<AxiosResponse<FcmLegacyApiResponse>> {
    return this.httpService
      .post<FcmLegacyApiResponse>(
        `${PUSH_END_POINT}/fcm/send`,
        payload,
        options,
      )
      .toPromise();
  }
}
