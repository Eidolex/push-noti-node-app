import { sign as jwtSign } from 'jsonwebtoken';
import { Injectable, HttpService } from '@nestjs/common';
import { FirebaseAuthResponse } from './interfaces/firebase-auth.interface';

const GOOGLE_TOKEN_AUDIENCE = 'https://accounts.google.com/o/oauth2/token';
const JWT_ALGORITHM = 'RS256';

const ONE_HOUR_IN_SECOND = 60 * 60;

@Injectable()
export class FirebaseAuthService {
  constructor(private readonly httpService: HttpService) {}

  // generate token of firebase
  public async getBearerToken(
    clientEmail: string,
    privateKey: string,
  ): Promise<string> {
    const token = FirebaseAuthService.createAuthJwt(clientEmail, privateKey);
    const payload =
      'grant_type=urn%3Aietf%3Aparams%3Aoauth%3A' +
      'grant-type%3Ajwt-bearer&assertion=' +
      token;

    try {
      const result = await this.httpService
        .post<FirebaseAuthResponse>(GOOGLE_TOKEN_AUDIENCE, payload, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        })
        .toPromise();

      return result.data.access_token;
    } catch (error) {
      console.log(error);
    }
  }

  // public async refreshBearerToken() {}

  private static createAuthJwt(
    clientEmail: string,
    privateKey: string,
  ): string {
    const claims = {
      scope: [
        //   'https://www.googleapis.com/auth/cloud-platform',
        //   'https://www.googleapis.com/auth/firebase.database',
        'https://www.googleapis.com/auth/firebase.messaging',
        //   'https://www.googleapis.com/auth/identitytoolkit',
        //   'https://www.googleapis.com/auth/userinfo.email',
      ].join(' '),
    };

    return jwtSign(claims, privateKey, {
      issuer: clientEmail,
      expiresIn: ONE_HOUR_IN_SECOND,
      audience: GOOGLE_TOKEN_AUDIENCE,
      algorithm: JWT_ALGORITHM,
    });
  }
}
