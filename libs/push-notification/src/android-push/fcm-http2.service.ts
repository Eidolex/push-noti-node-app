import { Injectable, HttpService } from '@nestjs/common';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import {
  FcmHttp2ApiHeader,
  FcmHttp2PushPayload,
  FcmHttp2ApiRequestBody,
  FcmHttp2ApiRequestData,
  FcmHttp2ApiResponse,
} from './interfaces/fcm-http2.interface';

const PUSH_END_POINT = 'https://fcm.googleapis.com';

@Injectable()
export class FcmHttp2Service {
  constructor(private readonly httpService: HttpService) {}

  public async push(
    { projectId, accessToken }: { projectId: string; accessToken: string },
    payload: FcmHttp2PushPayload[],
    options?: AxiosRequestConfig,
  ) {
    const headers = FcmHttp2Service.constructHeaders(accessToken);
    const data = FcmHttp2Service.constructPayload(projectId, payload);

    options = FcmHttp2Service.mergeOptions(headers, options);

    await this.sendToApi(data, options);
  }

  private static constructHeaders(accessToken: string): FcmHttp2ApiHeader {
    return {
      Authorization: `Bearer ${accessToken}`,
    };
  }

  private static constructPayload(
    projectId: string,
    payload: FcmHttp2PushPayload[],
  ): FcmHttp2ApiRequestBody {
    const data = payload.map<FcmHttp2ApiRequestData>(p => ({
      message: {
        token: p.deviceToken,
        data: {
          title: p.title,
          message: p.message,
          ...p.customKey,
        },
      },
    }));
    return {
      data,
      url: `${PUSH_END_POINT}/v1/projects/${projectId}/message:send`,
    };
  }

  private static mergeOptions(headers: any, options?: AxiosRequestConfig) {
    options = options || {};
    return {
      ...options,
      headers: {
        ...options.headers,
        ...headers,
      },
    };
  }

  private async sendToApi(
    payload: FcmHttp2ApiRequestBody,
    options: any,
  ): Promise<AxiosResponse<FcmHttp2ApiResponse>> {
    return this.httpService
      .post<FcmHttp2ApiResponse>(`${PUSH_END_POINT}/fcm/send`, payload, options)
      .toPromise();
  }
}
