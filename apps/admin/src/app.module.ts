import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AndroidSchedulePushModule } from './android-schedule-push/android-schedule-push.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [AndroidSchedulePushModule, AuthModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
