import { Injectable } from '@nestjs/common';
import { UserService } from '@app/push-dao';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService) {}

  public async validateUser(email: string, password: string) {
    const user = await this.userService.findUserByEmail(email);
    if (user && user.password === password) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}
