import { Module } from '@nestjs/common';
import { AndroidSchedulePushController } from './android-schedule-push.controller';
import { AndroidSchedulePushService } from './android-schedule-push.service';
import { PushDaoModule } from '@app/push-dao';

@Module({
  imports: [PushDaoModule],
  controllers: [AndroidSchedulePushController],
  providers: [AndroidSchedulePushService],
})
export class AndroidSchedulePushModule {}
