import { Test, TestingModule } from '@nestjs/testing';
import { AndroidSchedulePushService } from './android-schedule-push.service';

describe('AndroidSchedulePushService', () => {
  let service: AndroidSchedulePushService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AndroidSchedulePushService],
    }).compile();

    service = module.get<AndroidSchedulePushService>(AndroidSchedulePushService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
