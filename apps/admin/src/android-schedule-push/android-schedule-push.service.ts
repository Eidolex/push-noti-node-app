import { Injectable } from '@nestjs/common';
import {
  AndroidScheduleService,
  CreateAndroidScheduleDto,
  AndroidSchedule,
} from '@app/push-dao';

@Injectable()
export class AndroidSchedulePushService {
  constructor(
    private readonly androidScheduleService: AndroidScheduleService,
  ) {}

  public async createAndroidSchedule(
    data: CreateAndroidScheduleDto,
  ): Promise<AndroidSchedule> {
    try {
      return await this.androidScheduleService.createAndroidSechedule(data);
    } catch (error) {
      console.log(error);
    }
  }
}
