import { Test, TestingModule } from '@nestjs/testing';
import { AndroidSchedulePushController } from './android-schedule-push.controller';

describe('AndroidSchedulePush Controller', () => {
  let controller: AndroidSchedulePushController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AndroidSchedulePushController],
    }).compile();

    controller = module.get<AndroidSchedulePushController>(AndroidSchedulePushController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
