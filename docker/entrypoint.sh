#!/bin/sh
set -e

if [ "$SERVICE" == "worker" ]; then
    npm run build worker 
    exec npm run start:prod-worker
elif [ "$SERVICE" == "api" ]; then
    npm run build api
    exec  npm run start:prod-api
else
    npm run build 
    exec npm run start:prod-admin
fi