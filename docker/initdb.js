db = db.getSiblingDB('nest')
// add user to this database
db.createUser(
  {
    user: "root",
    pwd: "password",   // or cleartext password
    roles: [{ role: "readWrite", db: "nest" }]
  }
)